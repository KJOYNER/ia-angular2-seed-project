import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule, MatMenuModule, MatIconModule } from '@angular/material';

const modules = [
	CommonModule,
	FormsModule,
	ReactiveFormsModule,
	MatButtonModule,
	MatMenuModule,
	MatIconModule
];

@NgModule({
	imports: [ modules ],
	exports: [ modules ],
	declarations: [ ]
})
export class SharedModule { }
