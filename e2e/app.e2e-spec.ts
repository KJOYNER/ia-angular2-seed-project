import { AppPage } from './app.po';

describe('Angular Starter', () => {
  let page: AppPage;

  beforeEach(() => {
	page = new AppPage();
  });

  it('should display welcome message', () => {
	page.navigateTo();
	expect(page.getParagraphText()).toEqual('ANGULAR STARTER');
  });
});
